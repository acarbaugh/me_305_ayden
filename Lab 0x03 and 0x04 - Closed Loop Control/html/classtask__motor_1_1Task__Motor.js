var classtask__motor_1_1Task__Motor =
[
    [ "__init__", "classtask__motor_1_1Task__Motor.html#ae3b97feeaa8ca9a9eb7e00d8c927560a", null ],
    [ "closedloop", "classtask__motor_1_1Task__Motor.html#a6593107d63023b0931fe35b2907cf449", null ],
    [ "duty_share", "classtask__motor_1_1Task__Motor.html#a1e1b3dac5e3680c0f2e3fc4018ceb579", null ],
    [ "gain_share", "classtask__motor_1_1Task__Motor.html#a6ef12aa66af37ca9c4e628d339ed1168", null ],
    [ "mot_Flag", "classtask__motor_1_1Task__Motor.html#aaa02fcfbcdd7d42a84616b3404e61fba", null ],
    [ "motor_1", "classtask__motor_1_1Task__Motor.html#a1622bf5def9a9d9b344a3b4c9fbca0bb", null ],
    [ "motor_2", "classtask__motor_1_1Task__Motor.html#a6292dff591b13f8e304ba60d169adae7", null ],
    [ "motor_drv", "classtask__motor_1_1Task__Motor.html#ab9fbc76ed185384fab3826dbba806aa1", null ],
    [ "next_time", "classtask__motor_1_1Task__Motor.html#a8cd5c18886c0439db6ad25900802e531", null ],
    [ "omega1_share", "classtask__motor_1_1Task__Motor.html#a41f09dd08239cd78d3421c3f0ca0fc09", null ],
    [ "omega2_share", "classtask__motor_1_1Task__Motor.html#a879727ad3a3d5880b0749d17f65830b9", null ],
    [ "omega_share", "classtask__motor_1_1Task__Motor.html#ac6dce88e2086f49e0e26eae16799016c", null ],
    [ "period", "classtask__motor_1_1Task__Motor.html#a083bab479ac16bea926548029165c0de", null ],
    [ "runs", "classtask__motor_1_1Task__Motor.html#a02858b7aeec0760954e0bc1ab28ac537", null ]
];