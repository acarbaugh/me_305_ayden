'''
@file      task_data
@brief     This file contains the code for the data collection task
@author    Ayden Carbaugh
@author    Ryan Flaherty
@date      Dec 9, 2021
'''
# Import statements
import utime

class Task_Data:
    '''
    @brief    This class is responsible for collecting data.
    @details  This class collects and stroes position and velocity data from 
              the touchpanel task and angle and angular velocity data from the 
              IMU task.
    '''
    def __init__(self, period, xpos_share, ypos_share, xd_share, yd_share, z_share, angx_share, angy_share, omegax_share, omegay_share, data_Flag):
        '''
        @brief    This function initializes the data collection task.
        @details  This function initializes the data collection task by 
        defining all the shared variable and creating blank lists for each 
        piece of data to be stored.
        '''
        ## The shared x position
        self.xpos_share = xpos_share
        ## The shared y position
        self.ypos_share = ypos_share
        ## The shared x velocity
        self.xd_share = xd_share
        ## The shared y velocity
        self.yd_share = yd_share
        ## The shared z value
        self.z_share = z_share
        ## The shared x angle
        self.angx_share = angx_share
        ## The shared y angle
        self.angy_share = angy_share
        ## The shared x angular velocity
        self.omegax_share = omegax_share
        ## The shared y angular velocity
        self.omegay_share = omegay_share
        ## The flag variable for the data collection system
        self.data_Flag = data_Flag
        ## The frequency of the task
        self.period = period
        ## Blank list for x position
        self.xpos_list = []
        ## Blank list for y position
        self.ypos_list = []
        ## Blank list for x velocity
        self.xd_list = []
        ## Blank list for y velocity
        self.yd_list = []
        ## Blank list for x angle
        self.angx_list = []
        ## Blank list for y angle
        self.angy_list = []
        ## Blank list for x angular velocity
        self.omegax_list = []
        ## Blank list for y angular velocity
        self.omegay_list = []
        ## Blank list for time of data
        self.time_list = []
        ## The nu ber of iterations
        self.runs = 0
        ## The time for the next iteration
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
    
    def run(self):
        '''
        @brief    This function record all the data.
        @details  This function records all the data from the touchpanel and 
                  the IMU task.
        '''
        if (utime.ticks_us() >= self.next_time):
            if self.data_Flag.read() == 1:
                if self.runs == 0:
                    self.cur_time = utime.ticks_ms()
                self.time = utime.ticks_diff(utime.ticks_ms(), self.cur_time)
                
                self.xpos_list.append(self.xpos_share.read())
                    
                self.ypos_list.append(self.ypos_share.read())
                    
                self.xd_list.append(self.xd_share.read())
                    
                self.yd_list.append(self.yd_share.read())
                    
                self.angx_list.append(self.angx_share.read())
                    
                self.angy_list.append(self.angy_share.read())
                    
                self.omegax_list.append(self.omegax_share.read())
                    
                self.omegay_list.append(self.omegay_share.read())
                    
                self.time_list.append(self.time)
            
            elif self.data_Flag.read() == 2:
                for idx in range (len(self.xpos_list)):
                    print('x position [mm]: {:}, y position [mm]: {:},delta x [mm/s]: {:}, delta y [mm/s]: {:}, x angle [deg]: {:}, y angle [deg]: {:}, omega x [deg/s]: {:}, omega y [deg/s]: {:}, time [s]: {:} ]'.format(self.xpos_list[idx],self.ypos_list[idx],self.xd_list[idx],self.yd_list[idx],self.angx_list[idx],self.angy_list[idx],self.omegax_list[idx],self.omegay_list[idx],self.time_list[idx]/1000))
                self.data_Flag.write(0)
                self.xpos_list = []
                self.ypos_list = []
                self.xd_list = []
                self.yd_list = []
                self.angx_list = []
                self.angy_list = []
                self.omegax_list = []
                self.omegay_list = []
                self.time_list = []
                self.data_Flag.write(0)
            
            if self.data_Flag.read() == 0:
                self.runs = 0
                
            self.next_time += self.period