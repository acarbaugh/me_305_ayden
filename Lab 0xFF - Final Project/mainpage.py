'''@file                mainpage.py
   @brief               This file creates the main page for the project.
   @details             This file contains the text to generate the proper 
                        structure for the main page of the project. 

   @mainpage

   @section sec_intro   Introduction
                        This project es skills and code created over the course 
                        of the quarter to develop a device that will be used to 
                        balance a ball on a platform. This project required a 
                        touchpanel to determine the location and speed of the 
                        ball, an IMU to determine euler angles and angular 
                        speeds of the platform, and two motors with linkages to 
                        rotate the platform.
                        
   @section sec_task    Task Diagram
                        This image is the task diagram that is used to 
                        organize the system. 6 tasks were creatd, a touchpanel 
                        task, an IMU task, a data collection task, a controller 
                        task, a user taks, and a motor task. Each of the tasks 
                        will be explained intheir respective sections.
                        ![](taskdiagram.png)
                        
   @section main        Main Script
                        This file contains the main script which calls and runs
                        all the different tasks used in this project.
   
   @section user_task   User Task
                        The user task is responsible for interfacing with the 
                        user. It takes inputs form the user and comomunicates 
                        with the system to balance the ball or collect data. 
                        The state diagram can be seen below.
                        ![](ussd.png)
                        
   @section con_task    Controller Task
                        The controller task is responsible for retrieveing the 
                        ball speed and position data as well as the platform 
                        angle and angular velocity data. It then calculates the 
                        duty cycle required for each motor by applying closed 
                        loop control and proportional gain values. The state 
                        diagram for the controller task can be seen below.
                        ![](ctsd.png)
  
   @section mot_task    Motor Task
                        The motor task is responsible for interfacing with the 
                        motors. It recieves the duty cycle for each motor from 
                        the controller task and it applies it to the motors, so 
                        they spin at the desired speed. The state diagram for 
                        the motor task can be seen below.
                        ![](mtsd.png)
                        
   @section dat_task    Data Task
                        The data task is responsible for recording and storing 
                        the data recieved from the IMU and toucpanel tasks. It 
                        recieves ball position and velocity from the touchpanel 
                        task and angle and angular velocity data from the IMU 
                        task. Additionally, it recieves a flag variable from 
                        the user task indicating it should start recording 
                        data. The state diagram for the data task can be seen 
                        below.
                        ![](dtsd.png)
        
   @section IMU_task    IMU Task
                        The IMU task is responsible for interfacing with the 
                        IMU on the board. It retrieves the angle and angular 
                        speed of the platform and sends the data to the data 
                        collection task and the controller task. The state 
                        diagram for the IMU task can be seen below.
                        ![](IMUsd.png)
                        
   @section tou_task    Touchpanel Task
                        The touchpanel task is responsible for interfacing with 
                        touchpanel. It obtains ball position and velocity data 
                        and it sends it to the controller task and the data 
                        collection task. The state diagram for the touchpanel 
                        task can be seen below.
                        ![](tpsd.png)
                        
   @section share_class Shares Class
                        This script creates shared variables for tasks to 
                        interact with each other.
   @section video       Video Link
                        A video of the ball balancing can be seen at this link. https://cpslo-my.sharepoint.com/:v:/g/personal/acarbaug_calpoly_edu/EXb0RXSKbqJBsnvhFASgEAMB_1ndwJY4UyN7QVopC2cF2g?e=GH39a3and
                        A video of the data being collected can be seen at this link.https://cpslo-my.sharepoint.com/:v:/g/personal/acarbaug_calpoly_edu/EVcPUPk5iKpIpSWxT9k1A7cBlsng0aByYVFqJalwJ00tcQ?e=DvKC00
                        
   @section sec_code    Source Code Link
                        This link will direct you towards the file containing 
                        the source code for this project. https://bitbucket.org/acarbaugh/me_305_ayden/src/master/Lab%200xFF%20-%20Final%20Project/
   @author              Carbaugh, Flaherty

   @date                December 9, 2021
'''