'''
@file      task_IMU
@brief     This file contains the task for interacting with an IMU
@author    Ayden Carbaugh
@author    Ryan Flaherty
@date      Dec 9, 2021
'''
# Import statements
import BNO055
import utime
import os

class Task_IMU:
    '''
    @brief    This class is responsible for interfacing with an IMU.
    @details  This class interacts with an IMU by calibrating it and then 
              obtaining the euler angles and the angluar velocities.
    '''
    def __init__(self, period, angx_share, angy_share, omegax_share, omegay_share):
        '''
        @brief    This function initializes the IMU task
        @details  This functino initializes the IMU task by defining all the 
                  shared variables and calibrating the IMU
        '''
        ## The shared angle x of the platform
        self.angx_share = angx_share
        ## The shared angle y of the platform
        self.angy_share = angy_share
        ## The shared angular velocity x of the platform 
        self.omegax_share = omegax_share
        ## The shared angular velocity y of the platform
        self.omegay_share = omegay_share
        ## Initializes the IMU
        self.IMU = BNO055.BNO055()
        ## The frequency of the task
        self.period = period
        ## The number of iteratinos of the task
        self.runs = 0
        ## The time for the next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## Configuration mode for the IMU
        self.config = 0b0000
        ## NDOF mode for the IMU
        self.NDOF = 0b1100
        ## Calibrating the IMU
        if 'IMU_cal_coeffs' in os.listdir():
            print('Import Calibration Coefficients...')
            self.IMU.set_mode(self.config)
            with open('CalibrationCoefficients' , 'r') as f:
                self.IMU.set_cal(f.read())
            self.IMU.set_mode(self.NDOF)
        else:
            self.IMU.set_mode(self.NDOF)
            calstat = self.IMU.get_calstat()[0]
            print('calibrating')
            mag_calib = False
            acc_calib = False
            gyr_calib = False
            while calstat != 0b11111111:
                if ((calstat & 0b11) == 0b11) & (mag_calib == False):
                    print('magnetometer calibrated')
                    mag_calib = True
                if ((calstat & 0b1100) == 0b1100) & (acc_calib == False):
                    print('accelerometer calibrated')
                    acc_calib = True
                if ((calstat & 0b110000) == 0b110000) & (gyr_calib == False):
                    print('gyroscope calibrated')
                    gyr_calib = True
                calstat = self.IMU.get_calstat()[0]
            print('calibrated')
            
            cal = self.IMU.get_calcoeff()
            print(cal)
            cal_list = open('CalibrationCoefficients', 'w')
            cal_list.write(cal)
            cal_list.close()
    
    def run(self):
        '''
        @brief    This function finds angles and angluar velocities of the IMU.
        @details  This function finds the angles by recieving the data from the
                  IMU and assigning the each index to its assigned variable.
        '''
        if (utime.ticks_us() >= self.next_time):
            self.angles = self.IMU.get_euler()
            self.angx_share.write(self.angles[2])
            self.angy_share.write(self.angles[1])
            
            self.dangles = self.IMU.get_omega()
            self.omegax_share.write(self.dangles[2])
            self.omegay_share.write(self.dangles[1])
       
            self.next_time += self.period
            self.runs += 1