''' @file fibonacci_Ayden_Carbaugh.py
@brief This file asks for an index then gives the matching fibonacci number.
@details The file asks for an index as a number, validates it as an appropriate
         number, then outputs the corresponding fibonacci number.
@author Ayden Carbaugh
'''
def fib(idx):
    '''
    @brief     This function calculates a fibonacci number for the 
               specific index.
    @param idx An integer that specifies the index for the desired 
               fibonacci number.
    '''
    f0=0
    f1=1
    fib_list=[0,1]
    if idx == 0:
        return 0
    elif idx == 1:
        return 1
    for n in range(2,idx+1):
        f2=f0+f1
        f0=f1
        f1=f2
        fib_list.append(f2)
    return f2
 
   
if __name__== '__main__':
        while True:
            try:
                ## The index of the fibonacci number selected by the user.
                oldidx = input('Please enter a non-negative integer or type exit to exit the program: ')
                if oldidx == 'exit':
                    break
                else:
                    ## The index of the fibonacci number validated as a 
                    #  non-negative integer.
                    idx=int(oldidx)
            except ValueError:
                print ('Error, the number you entered was not a whole number.')
            else:
                if idx<0:
                    print ('Error, the number you entered was negative.')
                else: 
                    print ('Fibonacci number at ''index {:} is {:}.'.format(idx,fib(idx)))