'''
@file      task_panel
@brief     This file contains the code for the touchpanel task.
@author    Ayden Carbaugh
@author    Ryan Flaherty
@date      Dec 9, 2021
'''
## Import statements
import panel
import utime

class Task_Panel:
    '''
    @brief    This class is responsible for interfacing with the touchpanel.
    @details  This class interfaces with the touchpanel by retrieving the scan 
              data and converting it into x position, y position, x velocity, 
              and y velocity.
    '''
    def __init__(self, period, xpos_share, ypos_share, z_share, xd_share, yd_share):
        '''
        @brief    This function initializes the touchpanel task.
        @details  This function initializes the touchanel task by defining all 
                  the shared variables and the initial values used to calculate 
                  the velocity.
        '''
        ## The frequency of the task
        self.period = period
        ## The shared x position
        self.xpos_share = xpos_share
        ## The shared y position
        self.ypos_share = ypos_share
        ## The shared x velocity
        self.xd_share = xd_share
        ## The shared y velocity
        self.yd_share = yd_share
        ## The shared z value
        self.z_share = z_share
        ## Initializes the touchpanel
        self.panel = panel.Panel()
        ## Number of iterations of the task
        self.runs = 0
        ## The time of the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The start time of the x velocity calculation
        self.startx_time = utime.ticks_us()
        ## The start time of the y velocity calculation
        self.starty_time = utime.ticks_us()
        ## The starting x position for calculating the x velocity
        self.xpos1 = self.panel.xyzscan(1)
        ## The starting y position for calculating the y velocity
        self.ypos1 = self.panel.xyzscan(2)
        
    def run(self):
        '''
        @brief    This function creates the position and velocity data.
        @details  This function creates the x and y position data for the 
                  touchpanel as well as calculate the x and y velocity using 
                  timers and change in position.
        '''
        if (utime.ticks_us() >=self.next_time):
            self.z_pos = self.panel.xyzscan(3)
            self.z_share.write(self.z_pos)
            
            self.xpos2 = self.panel.xyzscan(1)
            self.ypos2 = self.panel.xyzscan(2)
            self.deltx = self.xpos2 - self.xpos1
            self.delty = self.ypos2 - self.ypos1
            
            self.xpos_share.write(self.xpos2)
            self.ypos_share.write(self.ypos2)
            
            self.stopx_time = utime.ticks_us()
            self.stopy_time = utime.ticks_us()
            self.dx_time = (self.stopx_time - self.startx_time)/1000000
            self.dy_time = (self.stopy_time - self.starty_time)/1000000
            
            self.xd_share.write(self.deltx/self.dx_time)
            self.yd_share.write(self.delty/self.dy_time)
            
            self.startx_time = self.stopx_time
            self.starty_time = self.stopy_time
            
            self.xpos1 = self.xpos2
            self.ypos1 = self.ypos2
        
        else:
            self.next_time += self.period
            self.runs += 1
            
            
            
            
            
            