'''
@file          main_script.py
@brief         This file calls the tasks required.
@details       This file calls the encoder task and the user interface task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
#Import statements
import task_data
import task_user
import task_controller
import task_IMU
import task_panel
import task_motor
import shares

def main():
    '''
    @brief   This script runs the user and encoder tasks
    @details This script initiializes the encoder and runs task_user, 
             task_encoder and task_motor.
    '''
    ## This variable creates a position which is shared between all the tasks
    xpos_share = shares.Share(0)
    ## This variable creates a position which is shared between all the tasks
    ypos_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    xd_share = shares.Share(0)
    ## This variable creates a delta which is shared between all the tasks
    yd_share = shares.Share(0)
    ## This variable creates a duty cycle which is shared between all the tasks
    z_share = shares.Share(0)
    ## This variable creates an omega which is shared between all the tasks
    angx_share = shares.Share(0)
    ## This variable creates a measured omega for motor 1 which is shared between all the tasks
    angy_share = shares.Share(0)
    ## This variable creates a measured omega for motor 2 which is shared between all the tasks
    omegax_share = shares.Share(0)
    ## This variable creates a gain value which is shared between all the tasks
    omegay_share = shares.Share(0)
    ## This variable creates a shared duty cycle for motor 1
    duty_1 = shares.Share(0)
    ## This variable creates a shared duty cycle for motor 2
    duty_2 = shares.Share(0)
    ## This variable creates a flag for the current state which is shared between all the tasks
    cont_Flag = shares.Share(0)
    ## This variable creates a flag for the current state which is shared between all the tasks
    data_Flag = shares.Share(0)
    ## Defines task 1 as the user task
    task1 = task_user.Task_User(10_000, cont_Flag, data_Flag)
    ## Defines task 2 as the panel task
    task2 = task_panel.Task_Panel(10_000, xpos_share, ypos_share, z_share, xd_share, yd_share)
    ## Defines task 3 as the IMU task
    task3 = task_IMU.Task_IMU(10_000, angx_share, angy_share, omegax_share, omegay_share)
    ## Defines task 4 as the controller task
    task4 = task_controller.Task_Controller(10_000, xpos_share, ypos_share, z_share, xd_share, yd_share, angx_share, angy_share, omegax_share, omegay_share, duty_1, duty_2, cont_Flag)
    ## Defines task 5 as the motor task
    task5 = task_motor.Task_Motor(10_000, duty_1, duty_2)
    ## Defines task 6 as the data task
    task6 = task_data.Task_Data(10_000, xpos_share, ypos_share, xd_share, yd_share, z_share, angx_share, angy_share, omegax_share, omegay_share, data_Flag)
    ## Creates a list to run both tasks while in a while loop
    task_list = [task1, task2, task3, task4, task5, task6]

    while(True):
       try:
          for task in task_list:
              task.run() 
       except KeyboardInterrupt:
           break
           print('Program Terminating')

if __name__ == '__main__':
    main()
            