'''
@file      task_controller
@brief     This file contains the code for the controller task.
@author    Ayden Carbaugh
@author    Ryan Flaherty
@date      Dec 9, 2021
'''
# Import statements
import closedloop
import utime

class Task_Controller:
    '''
    @brief    This class contains the functions to create a controlled loop.
    @details  This class contains the functions to create a control loop by 
               \applying gain and voltages to the given position data and 
               multiplying it by the torque.
    '''
    def __init__(self, period, xpos_share, ypos_share, z_share, xd_share, yd_share, angx_share, angy_share, omegax_share, omegay_share, duty_1, duty_2, cont_Flag):
        '''
        @brief   This function initializes the controller task
        @details This function initializes the controller task by creating 
                 shared variables and initializing the closeloop driver.
        '''
        ## The time of each iteration
        self.period = period
        ## The shared x position
        self.xpos_share = xpos_share
        ## The shared y position
        self.ypos_share = ypos_share
        ## The shared x velocity
        self.xd_share = xd_share
        ## The shared y velocity
        self.yd_share = yd_share
        ## The shared z value
        self.z_share = z_share
        ## The shared x angle
        self.angx_share = angx_share
        ## The shared y angle
        self.angy_share = angy_share
        ## The shared x angular velocity
        self.omegax_share = omegax_share
        ## The shared y angular velocity
        self.omegay_share = omegay_share
        ## The shared contoller flag
        self.cont_Flag = cont_Flag
        ## The shared duty cycle for motor 1
        self.duty_1 = duty_1
        ## The shared duty cycle for motor 2
        self.duty_2 = duty_2
        ## Initializes the x controller
        self.cont_x = closedloop.ClosedLoop([-0.3, -10, -0.05, -2])
        ## Initializes the y controller
        self.cont_y = closedloop.ClosedLoop([-0.3, 10, -0.05, 2])
        ## The time for the next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The number of iterations
        self.runs = 0
        ## The reference vector for closed loop control
        self.ref_vector = [0,0,0,0]
        
    def run(self):
        '''
        @brief   This function creates a sgared duty cycle for the motor task.
        @details This function creates a shared duty cycle by applying gain 
                 torque and other constants to the data supplied by the 
                 touchpanel and IMU task.
        '''
        if (utime.ticks_us() >= self.next_time):
            if self.cont_Flag.read() == 1:
                if self.z_share.read() == True:
                    self.cont_x.set_kp([-1,-5,-.1,-.03])
                    self.cont_y.set_kp([1.5,5,.1,.1])
                    self.meas_vectorx = [self.xpos_share.read(), self.angx_share.read(), self.xd_share.read(), self.omegax_share.read()]
                    self.meas_vectory = [self.ypos_share.read(), self.angy_share.read(), self.yd_share.read(), self.omegay_share.read()]
                    self.duty_1.write(self.cont_x.update(self.ref_vector, self.meas_vectorx))
                    self.duty_2.write(self.cont_y.update(self.ref_vector, self.meas_vectory))
                    
                elif self.z_share.read() == False:
                    self.cont_x.set_kp([0,-7.5,0,-0.03])
                    self.cont_y.set_kp([0,9.5,0,0.1])
                    self.meas_vectorx = [0, self.angx_share.read(), 0, self.omegax_share.read()]
                    self.meas_vectory = [0, self.angy_share.read(), 0, self.omegay_share.read()]
                    self.duty_1.write(self.cont_x.update(self.ref_vector, self.meas_vectorx))
                    self.duty_2.write(self.cont_y.update(self.ref_vector, self.meas_vectory))
                    pass
        self.next_time += self.period
        self.runs += 1