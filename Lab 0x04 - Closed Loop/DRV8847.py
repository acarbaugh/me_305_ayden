'''
@file        DRV8847.py
@brief
@author      Ayden Carbaugh
@author      Ryan Flaherty
@date        Nov 16, 2021
'''
import pyb
import utime
class DRV8847:
    '''
    @brief 
    @details
    '''
    def __init__(self):
        '''
        @brief      Initialized and returns a DRV8847 object.
        @details    
        @param
        @return
        '''
        self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.B2 = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN)
        self.nFAULT = pyb.ExtInt(self.B2, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
        
        pass
    def enable(self):
        '''
        @brief      Takes DRV8847 out of sleep mode.
        @details    this function takes DRV8847 out of sleep mode by setting 
                    the nSLEEP pin to low.
        '''
        self.nFAULT.disable()
        self.nSLEEP.low()
        utime.sleep_us(25)
        self.nFault.enable()
        pass
    
    def disable(self):
        '''
        @brief      Puts DRV8847 into sleep mode.
        @details    This function puts DRV8847 into sleep mode by setting the 
                    nSLEEP pin to high
        '''
        self.nSLEEP.high()
        pass
    
    def fault_cb(self, IRQ_src):
        '''
        @brief      
        '''
        self.button = 1
        self.nSLEEP.high()
        print('A Fault was detected, program stopped.')
        pass
    
    def motor(self, motor):
        return Motor(motor)

class Motor:
    
    def __init__(self, motor):
        self.IN1 = pyb.Pin(pyb.Pin.cpu.B4)
        self.IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
        self.tim3 = pyb.Timer(3, freq = 20000)
        self.t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.IN1)
        self.t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.IN2)
        self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
        self.IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
        self.t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.IN3)
        self.t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.IN4)
        self.motor = motor
        
    def set_duty(self, duty):
        if self.motor == 1:
            if duty > 0:
                self.t3ch1.pulse_width_percent(abs(duty))
                self.t3ch2.pulse_width_percent(0)
            elif duty < 0:
                self.t3ch1.pulse_width_percent(0)
                self.t3ch2.pulse_width_percent(abs(duty))    
        pass
    
        if self.motor == 2:
            if duty > 0:
                self.t3ch3.pulse_width_percent(abs(duty))
                self.t3ch4.pulse_width_percent(0)
            elif duty < 0:
                self.t3ch3.pulse_width_percent(0)
                self.t3ch4.pulse_width_percent(abs(duty)) 
        pass
# if __name__ == '__main__':
    
#     motor_drv = DRV8847()
#     motor_1 = motor_drv.motor()
#     motor_2 = motor_drv.motor()
    
#     motor_drv.enable()
    
#     motor_1.set_duty(40)
#     motor_2.set_duty(60)
    

        
        
        
        
        
        
        

        