'''
@file          task_motor.py
@brief         This file creates the class for interacting with the motor.
@details       This file creates a class for interacting with a motor to get 
               the desired output for the input provided by the user task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
import DRV8847
import utime

class Task_Motor:
    ''' 
    @brief     This class creates a task for interfacing with a motor object.
    @details   This class creates a task for interfacing with a motor objects
               using shares values from other scripts.
    '''    
    def __init__(self, period, duty_1, duty_2):
        '''
        @brief     This function initializes the motor task.
        @details   This function initializes the task by impolementing shared 
                   variables.
        '''
        ## The shared period of each iteration
        self.period = period
        ## The duty cycle for motor 1
        self.duty_1 = duty_1
        ## The duty cycle for motor 2
        self.duty_2 = duty_2
        ## The time for next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The number of runs through the code
        self.runs = 0
        ## Initializes the motor object
        self.motor = DRV8847.Motor()
        
    def run(self):
        '''
        @brief     This function interacts with motor driver.
        @details   This function interacts with the motor driver by setting the 
                   motor's PWM.
        '''
        if (utime.ticks_us() >= self.next_time):
            self.motor.set_duty(1,self.duty_1.read())  
            self.motor.set_duty(2,self.duty_2.read())                
    
            self.next_time += self.period
            self.runs +=1