'''
@file          task_user.py
@brief         This file creates the class for interacting with the user.
@details       This file creates a class for interacting with a user to get the
               desired output for the input provided.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import pyb
import utime
## Defines the state of the finite state machine as state 0
s0_init = 0
## Defines the state of the finite state machine as state 1
s1 = 1
## Defines the state of the finite state machine as state 2
s2 = 2
## Defines the state of the finite state machine as state 3
s3 = 3
class Task_User:
    '''@brief      This class interacts with an encoder task.
       @details    This class interacts with the user task to prompt a user
                   for their desired inpormation, then displays that
                   information after interacting with the encoder task.
    '''
    def __init__(self, period, position_share, delta_share, enc_Flag):
        '''
        @brief     This function initializes the user task.
        @details   This function initializes the task by defining the state,
                   and creating an encoder variable.
        @param     s0_init the initial state.
        @return    returns the encoder variable.
        '''
        ## The postion of encoder 1, shared between multiple tasks
        self.position_share = position_share
        ## The delta of encoder 1, shared between multiple tasks
        self.delta_share = delta_share
        ## Sets the intiial state of the finite state machine as state 0
        self.state = s0_init
        ## The period of the task
        self.period = period
        ## Number of runs of the finite state machine
        self.runs = 0
        ## Serial port for user interfacing
        self.ser = pyb.USB_VCP()
        ## Time to run the next iteration of the task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## A flag for the current state which is shared between all the tasks
        self.enc_Flag = enc_Flag
        ## Starts the enc flag at state 0
        self.enc_Flag.write(0)
        ## Variable used to index through lists
        self.idx = 0
        ## List used to display encoder 1 position while collecting data
        self.position_list = []
        ## List used to display time while collecting data
        self.time_list = []
        ## The period used to collect data during data collection
        self.time_period = 100
        ## The time to run the next iteration state 2
        self.next_time2= utime.ticks_add(utime.ticks_ms(),self.time_period)
        ## The reference time for beginning state 2
        self.Timref = 0
        
    def run(self):
        '''
        @brief     This function runs the user task.
        @details   This function runs the task by inputing instructions and
                   and prompting a key command input.
        @return    returns the desired output.
        '''
        ## User interface instructions
        self.instruct = """
                  "Instructions - press one of the following keys their respective functions: \n"
                  "z:   Zero the position of encoder 1\n"
                  "p:   Print the position of encoder 1\n"
                  "d:   Print the delta for encoder 1\n"
                  "g:   Collect data for encoder 1 for 30 seconds and print it as list\n"
                  "s:   End data collection prematurely\n"
                  "h:   Redisplay user command interface")
            """
        ## Current time of the iteration
        current_time = utime.ticks_us()    
        
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            if self.state == s0_init:
                print(self.instruct)
                self.state = s1
            elif self.state == s1:
                self.enc_Flag.write(0)
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'z':
                        self.enc_Flag.write(1)
                        print('Position zeroed')
                    elif self.char_in == 'p':
                        print('Position of encoder 1 is {:}'.format(self.position_share.read()))
                    elif self.char_in == 'd':
                        self.enc_Flag.write(2)
                        print('Encoder 1 delta is {:}'.format(self.delta_share.read()))
                    elif self.char_in =='g':
                        self.state = s2
                        self.Timref = utime.ticks_ms()
                        self.end_30s = utime.ticks_add(self.Timref, 30000)
                        print('Collecting data for encoder 1')
                    elif self.char_in == 'h':
                        print(self.instruct)
                        pass
            elif self.state == s2:
                self.enc_Flag.write(0)
                self.idx = 0
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 's':
                        self.state = s3
                if utime.ticks_ms() >= self.end_30s:
                    self.state = s3
                if utime.ticks_ms() >= self.next_time2:
                    self.position_share.read()
                    self.position_list.append(self.position_share.read())
                    self.time_list.append(utime.ticks_diff(utime.ticks_ms(), self.Timref))
                    self.next_time2 = utime.ticks_add(utime.ticks_ms(), self.time_period)
            elif self.state == s3:
                for idx in range (len(self.position_list)):
                    print('[{:},{:}]'.format(self.position_list[idx],self.time_list[idx]/1000))
                self.state = s1
                self.position_list = []
                self.time_list = []
            else:
                raise ValueError('Invalid state')
                
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1