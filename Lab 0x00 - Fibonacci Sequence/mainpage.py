'''@file                mainpage.py
   @brief               This file creates the main page for the project.
   @details             This file contains the text to generate the proper structure for the main page of the project. 

   @mainpage

   @section sec_intro   Introduction
                        This project creates an file that will determine the 
                        fibonacci number for an index specified by the user. 
                        If the user inputs a non-accepted index, the program
                        will display an error message and repromt the user to 
                        input a valid index.
   
   @section sec_fib     Fibonacci
                        This file contains the code that generates the 
                        fibonacci number for the specified index.
   @section sec_flink   Source Code Link
                        This is the link to access the source code for Lab_0x00.
                        
   @author              Carbaugh

   @date                October 8, 2021
'''