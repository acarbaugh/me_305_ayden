


class ClosedLoop:
    def __init__(self, delta_share, omega_ref, kp_share):
        
        self.omega_ref = omega_ref
        self.delta_share = delta_share
        self.kp_share = kp_share
        self.err = [0]
        self.err_sum = 0
        self.err_prev = self.omega_ref.read() - self.delta.read()
        
    def run(self):
        
        self.err = self.omega_ref.read() - self.delta_share.read()
        self.err_sum += self.err*(1/100)
        self.err_diff = self.err - self.err_prev
        self.err_prev = self.err
        PID = self.kp_share.read()*(self.err) + 1*self.err_sum + 1*self.err_diff
        self.duty = PID/3.3
        if self.duty > 100:
            self.duty = 100
        return self.duty
        
    def get_kp(self,kp):
        
        return self.kp
    
    def set_kp(self, kp):
        self.kp = kp
        