'''
@file          encoder1.py
@brief         This file creates the class for interacting with an encoder.
@details       This file creates a class for interacting wiith a quadrature 
               encoder. It does this through the use of an initialization, 
               update, get postiion, get delta, and set position functions.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import pyb

class Encoder:
    ''' 
    @brief     This class creates and encoder object.
    @details   This class creates an encoder object to interface with a 
               quadrature encoder, which can be used in future tasks.
    '''
    def __init__(self):
        ''' 
        @brief     This function builds an encoder object.
        @details   This function defines the position of the encoder by 
                   defining the timer and setting up the position.
        @param     pyb.Timer timer for the encoder.
        @return    this intializes the encoder for future use.
        ''' 
        ## The initial position of encoder 1 
        self.position = 0
        ## The intial delta of encoder 1
        self.delta = 0
        ## The A side pin for encoder 1
        self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        ## The B side pin for encoder 1
        self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
        ## The timer for encoder 1
        self.tim4 = pyb.Timer(4, prescaler = 0, period = 65535)
        ## Channel 1 for timer 4
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_A, pin=self.pinB6)
        ## Channel 2 for timer 4
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_B, pin=self.pinB7)
        ## Define the encoder start position
        self.start = 0
        ## Defines the encoder end position
        self.enccount_1 = 0
        
    def update(self):
        ''' 
        @brief      This function updates the encoder's position and delta.
        @details    This function updates the current position and change in
                    position for the encoder for everytime it is rotated.
        @param      enccount_1 this is the enoder counter.
        @return     returns the current position and delta.
        '''
        self.enccount_1 = abs(self.tim4.counter())
        self.delta = self.enccount_1 - self.start
        if self.delta > 65535/2:
            self.delta = self.delta - 65535
        if self.delta < (-1*65535/2):
            self.delta = self.delta + 65535
        self.start = self.enccount_1
        self.position += self.delta
        
    def get_position(self):
        '''
        @brief      This function returns the encoder's position.
        @details    This function returns the curent position of the ecnoder.
        @param      current_pos This fuction uses current position as an input.
        @return     This function returns the current position.
        '''
        return self.position
    
    
    def set_position(self, position):
        '''
        @brief      This function updates the encoder's position.
        @details    This function updates the encoder position when called.
        @param      enccount_1 This function uses the encoder counter.
        @return     This function reutrns the new position.
        '''
        self.start = abs(self.tim4.counter())
        self.position = position
    
    def get_delta(self):
        '''
        @brief      This function finds updates the delta of the encoder. 
        @details    This function finds the cahneg in position of the encoder.
        @param      self.delta This function takes delta as an input.
        @return     This function returns the current delta.
        '''
        return self.delta