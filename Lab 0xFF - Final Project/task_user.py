'''
@file      task_user
@brief     This file contains the code for the user interface task.
@author    Ayden Carbaugh
@author    Ryan Flaherty
@date      Dec 9, 2021
'''
# Import statements
import utime
import pyb
## State 0
s0 = 0
## State 1
s1 = 1
class Task_User:
    '''
    @brief    This class is responsible for interfacing with the user.
    @details  This class interfaces with the user by prompting keyboard inputs 
              which correspond to different actions.
    '''
    def __init__(self, period, data_Flag, cont_Flag):
        '''
        @brief    This function initializes the user task.
        @details  This function initializes the user task by defining flag 
                  varibles and creating a recognizable input form the user.
        '''
        ## Frequency of the task
        self.period = period
        ## Flag variable for the data collection task
        self.data_Flag = data_Flag
        ## Flag variable for the controller task
        self.cont_Flag = cont_Flag
        ## Number of iterations of the task
        self.runs = 0
        ## Time for the next iteration
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## State of the task
        self.state = s0
        ## Communication with user
        self.ser = pyb.USB_VCP()
        
    def run(self):
        '''
        @brief    This function prompts the user to input a command.
        @details  This functino prompts the user to input a command which is 
                  then processed and relayed to the controller or data 
                  collection task.
        '''
        self.instruct = """
-------------------------------------------------------------------------------    
Instructions - press one of the following keys for their respective functions:
-------------------------------------------------------------------------------
b: balance the ball
c: clear motor fault
d: collect data
e: end data collection
i: redisplay the instructions
-------------------------------------------------------------------------------
                  """ 
            
        current_time = utime.ticks_us()          
        if utime.ticks_diff(current_time, self.next_time) >= 0:
            if self.state == s0:
                print(self.instruct)
                self.state = s1
                
            elif self.state == s1:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    if self.char_in == 'b':
                        print('Balancing the ball ... ')
                        self.cont_Flag.write(1)
                    #elif self.char_in == 'c'
                        #self.mot_Flag.write(3)
                        #print('Fault cleared')
                    elif self.char_in == 'd':
                        print('data is being collected ...')
                        self.data_Flag.write(1)
                    elif self.char_in == 'e':
                        self.data_Flag.write(2)
                    elif self.char_in == 'i':
                        self.state = 0
                    pass
                        
            else:
                raise ValueError('Invalid state')
                            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1