'''
@file          encoder.py
@brief         This file creates the class for interacting with an encoder.
@details       This file creates a class for interacting wiith a quadrature 
               encoder. It does this through the use of an initialization, 
               update, get postiion, get delta, and set position functions.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import pyb

class Encoder:
    ''' 
    @brief     This class creates an encoder object.
    @details   This class creates an encoder object to interface with a 
               quadrature encoder, which can be used in future tasks.
    '''
    def __init__(self):
        ''' 
        @brief     This function builds an encoder object.
        @details   This function defines the position of the encoder by 
                   defining the timer and setting up the position.
        ''' 
        ## The initial position of encoder 1 
        self.position1 = 0
        ## The initial position of encoder 2 
        self.position2 = 0
        ## The intial delta of encoder 1
        self.delta1 = 0
        ## The intial delta of encoder 2
        self.delta2 = 0
        ## The A side pin for encoder 1
        self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        ## The B side pin for encoder 1
        self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
        ## The timer for encoder 1
        self.tim4 = pyb.Timer(4, prescaler = 0, period = 65535)
        ## Channel 1 for timer 4
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_A, pin=self.pinB6)
        ## Channel 2 for timer 4
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_B, pin=self.pinB7)
        ## The A side pin for encoder 2
        self.pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
        ## The B side pin for encoder 2
        self.pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
        ## The timer for encoder 2
        self.tim8 = pyb.Timer(8, prescaler = 0, period = 65535)
        ## Channel 1 for timer 8
        self.t8ch1 = self.tim8.channel(1, pyb.Timer.ENC_A, pin=self.pinC6)
        ## Channel 2 for timer 8
        self.t8ch2 = self.tim8.channel(2, pyb.Timer.ENC_B, pin=self.pinC7)
        ## Define the encoder 1 start position
        self.start1 = 0
        ## Define the encoder 2 start position
        self.start2 = 0
        ## Defines the encoder 1 end position
        self.enccount1 = 0
        ## Defines the encpder 2 end position
        self.enccoun2 = 0
        
    def update(self):
        ''' 
        @brief      This function updates the encoder's position and delta.
        @details    This function updates the current position and change in
                    position for the encoder for everytime it is rotated.
        @param      enccount_1 this is the enoder counter.
        @return     returns the current position and delta.
        '''
        self.enccount1 = abs(self.tim4.counter())
        self.delta1 = self.enccount1 - self.start1
        if self.delta1 > 65535/2:
            self.delta1 = self.delta1 - 65535
        if self.delta1 < (-1*65535/2):
            self.delta1 = self.delta1 + 65535
        self.start1 = self.enccount1
        self.position1 += self.delta1
        
        self.enccount2 = abs(self.tim8.counter())
        self.delta2 = self.enccount2 - self.start2
        if self.delta2 > 65535/2:
            self.delta2 = self.delta2 - 65535
        if self.delta2 < (-1*65535/2):
            self.delta2 = self.delta2 + 65535
        self.start2 = self.enccount2
        self.position2 += self.delta2
        
    def get_position(self, encoder):
        '''
        @brief      This function returns the encoder's position.
        @details    This function returns the curent position of the ecnoder.
        @param      current_pos This fuction uses current position as an input.
        @return     This function returns the current position.
        '''
        if encoder == 1:
            return self.position1
        elif encoder == 2:
            return self.position2
    
    
    def set_position(self, position1, position2, encoder):
        '''
        @brief      This function updates the encoder's position.
        @details    This function updates the encoder position when called.
        @param      enccount_1 This function uses the encoder counter.
        @return     This function reutrns the new position.
        '''
        if encoder == 1:
           self.start1 = abs(self.tim4.counter())
           self.position1 = position1
        elif encoder == 2:
            self.start2 = abs(self.tim8.counter())
            self.position2 = position2
        
    def get_delta(self, encoder):
        '''
        @brief      This function finds updates the delta of the encoder. 
        @details    This function finds the cahneg in position of the encoder.
        @param      self.delta This function takes delta as an input.
        @return     This function returns the current delta.
        '''
        if encoder == 1:
            return self.delta1
        elif encoder == 2:
            return self.delta2