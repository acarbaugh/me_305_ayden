
import DRV8847
import utime

class Task_Motor:
    
    def __init__(self, period, duty_share, mot_Flag):
        self.mot_Flag = mot_Flag
        self.period = period
        self.duty_share = duty_share
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.runs = 0
        self.motor_drv = DRV8847.DRV8847()
        self.motor_1 = self.motor_drv.motor(1)
        self.motor_2 = self.motor_drv.motor(2)
        
    def run(self):
        if utime.ticks_us() >= self.next_time:
            if self.mot_Flag.read() == 1:
                self.motor_drv.enable()
                self.motor_1.set_duty(self.duty_share.read())
                self.mot_Flag.write(0)
                
            elif self.mot_Flag.read() == 2:
                self.motor_drv.enable()
                self.motor_2.set_duty(self.duty_share.read())
                self.motor_flag.write(0)
                
            elif self.mot_Flag.read() == 3:
                self.motor_drv.enable()
                
            else:
                
                self.next_time += self.period
                self.runs +=1