'''
@file          BNO055.py
@brief         This file creates a driver for interacting with an IMU.
@details       This file craeates a driver for interacting with an IMU by 
               obtaining the calibration coefficients and using them to obtaian 
               the orientation data.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Nov 1, 2021
'''
# Import statements
from pyb import I2C
import struct

class BNO055:
    ''' 
    @brief     This class creates a BNO055 object.
    @details   This class creates a BNO055 object to interface with an IMU to be 
               used in a task.
    '''
    def __init__(self):
        self.i2c = I2C(1, I2C.MASTER)
        
    def set_mode(self, mode):
        '''
        @brief      Sets mode of IMU to NDOF
        @details    This function sets the operation mode of the IMU to NDOF
                    mode. 
        '''
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def get_calstat(self):
        '''
        @brief      Sets mode of IMU to NDOF
        @details    This function sets the operation mode of the IMU to NDOF
                    mode. 
        '''
        return self.i2c.mem_read(1, 0x28, 0x35)
    
    def get_calcoeff(self):
        '''
        @brief      Sets mode of IMU to NDOF
        @details    This function sets the operation mode of the IMU to NDOF
                    mode. 
        '''
        return self.i2c.mem_read(22, 0x28, 0x55)
    
    def set_cal(self, cal_coeff):
        '''
        @brief      Sets mode of IMU to NDOF
        @details    This function sets the operation mode of the IMU to NDOF
                    mode. 
        '''
        self.i2c.mem_write(cal_coeff, 0x28, 0x1A)
        
    def get_euler(self):
        '''
        @brief      Sets mode of IMU to NDOF
        @details    This function sets the operation mode of the IMU to NDOF
                    mode. 
        '''
        self.eul_ang = struct.unpack('<hhh', self.i2c.mem_read(6, 0x28, 0x1A))
        self.eul_deg = tuple(self.eul_int/16 for self.eul_int in self.eul_ang)
        return self.eul_deg
    
    def get_omega(self):
        '''
        @brief      Sets mode of IMU to NDOF
        @details    This function sets the operation mode of the IMU to NDOF
                    mode. 
        '''
        self.omega_ang = struct.unpack('<hhh', self.i2c.mem_read(6, 0x28, 0x14))
        self.omega_deg = tuple(self.omega_int/16 for self.omega_int in self.omega_ang)
        return self.omega_deg

# if __name__== '__main__':
#     IMU = BNO055()
#     config = 0b0000
#     NDOF = 0b1100
#     if 'CalibrationCoefficients' in os.listdir() :
#         print('Import Calibration Coefficients...')
#         IMU.set_mode(config)
#         with open('CalibrationCoefficients', 'r') as f:
#             IMU.set_cal(f.read())
#         IMU.set_mode(NDOF)
#     else:
#         # initial Calibration
#         IMU.set_mode(NDOF)
#         calstat = IMU.get_calstat()[0]
#         print("calibrating:")
#         mag_calib = False
#         acc_calib = False
#         gyr_calib = False
#         while calstat != 0b11111111:
#             if ((calstat & 0b11) == 0b11) & (mag_calib == False):
#                 print("magnetometer calibrated")
#                 mag_calib = True
#             if ((calstat & 0b1100) == 0b1100) & (acc_calib == False):
#                 print("accelerometer calibrated")
#                 acc_calib = True
#             if ((calstat & 0b110000) == 0b110000) & (gyr_calib == False):
#                 print("gyroscope calibrated")
#                 gyr_calib = True
#             calstat = IMU.get_calstat()[0]
#         print("Calibrated")
        
        
#         # print calibration data to file
#         calib = IMU.get_calcoeff()
#         print(calib)
#         calib_list = open('CalibrationCoefficients', 'w')
#         calib_list.write(calib)
#         calib_list.close()
    

#     print('(Heading, Pitch, Roll) deg:')
    
#     while True:
#         angles = IMU.get_euler()
#         print(angles)
#         omega = IMU.get_omega()
#         print(omega)
#         time.sleep(1)
        
        