'''
@file          closedloop.py
@brief         This file creates the class for creating closed loop control.
@details       This file creates the class for creating a closed loop 
               controller using a proportional gain.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
class ClosedLoop:
    ''' 
    @brief     This class creates a task to implement closed loop control.
    @details   This class creates a task to implement closed loop control using
               a proportional input value.
    '''    
    def __init__(self, gain_vector):
        '''
        @brief     This function initializes the closedloop driver.
        @details   This function initializes the closedloop  driver by creating
                   measured and referenced omega values.
        '''
        self.gain_vector = gain_vector
        
    def update(self, ref_vector, meas_vector):
        '''
        @brief     This function runs the closedloop task.
        @details   This function runs the closedloop task by taking referenced
                   and measured velocity data and modifying that actuation of
                   the motor.
        @return    self.actuation
        '''
        self.ref_vector = ref_vector
        self.meas_vector = meas_vector
        self.max_lim = 100
        self.min_lim = -100
        self.actuation = 0
        self.R = 2.21
        self.K_t = 13.8
        self.V_dc = 12  
        
        for self.idx in range(4):
            self.actuation += self.gain_vector[self.idx]*(self.ref_vector[self.idx]-self.meas_vector[self.idx])

        self.duty = ((100*self.R)/(4*self.K_t*self.V_dc))*self.actuation
        if self.duty >= self.max_lim:
            self.duty = self.max_lim
        elif self.duty <= self.min_lim:
            self.duty = self.min_lim
        return self.duty
        
    def get_kp(self):
        '''
        @brief     This function returns the gain value.
        @details   This function returns the gain value to be used by tasks.
        @return    gain_share
        '''
        return self.gain_vector
    
    def set_kp(self, gain_vector):
        '''
        @brief     This function sets the gain value.
        @details   This functionsets the gain value to be used by tasks.
        '''
        self.gain_vector = gain_vector
        