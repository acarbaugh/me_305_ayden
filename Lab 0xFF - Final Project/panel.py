'''
@file          main_script.py
@brief         This file calls the tasks required.
@details       This file calls the encoder task and the user interface task.
@author        Ayden Carbaugh
@author        Ryan Flaherty
@date          Oct 18, 2021
'''
# Import statements
import pyb
import utime


class Panel:
    '''
    @brief     This class interfaces with the touchpanel to obtain the position.
    @details   This class interfaces with a toupanel by scanning the x 
               position, y position, and z position to determine if an object 
               is touching the panel and its exact location.
    '''
    def __init__(self):
        '''
        @brief     This function initializes the touchpanel.
        @details   This function initializes the touchpanel by creating pins
                   the different voltage measurement locations and assigns 
                   values to the length, width, and center location of the 
                   panel.
        '''
        ## Creates a pin for xm
        self.Pinxm = pyb.Pin(pyb.Pin.cpu.A1)
        ## Creates a pin for ym
        self.Pinym = pyb.Pin(pyb.Pin.cpu.A0)
        ## Creates a pin for xp
        self.Pinxp = pyb.Pin(pyb.Pin.cpu.A7)
        ## Creates a pin for yp
        self.Pinyp = pyb.Pin(pyb.Pin.cpu.A6)
        ## Length of the panel in the x direction
        self.xdist = 176
        ## Length of the panel in the y direction
        self.ydist = 100
        ## The center of the panel in the x direction
        self.xcdist = 86
        ## The center of the panel in the y direction
        self.ycdist = 45
        
    def xyzscan(self, coordinate):
        '''
        @brief     This function performs an x, y, and z scan of the touchpanel.
        @details   This function performs an x, y, and z scan by setting the 
                   pins to different voltages and measuring the voltage at a 
                   pin not set with an ADC.
        @return    xpos, ypos, z
        '''
        if coordinate == 1:
            self.xp = pyb.Pin(self.Pinxp, pyb.Pin.OUT_PP)
            self.xp.high()
            self.xm = pyb.Pin(self.Pinxm, pyb.Pin.OUT_PP)
            self.xm.low()
            self.yp = pyb.Pin(self.Pinyp, pyb.Pin.IN)
            self.ym = pyb.Pin(self.Pinym, pyb.Pin.IN)
            self.ADC_ym = pyb.ADC(self.Pinym)
            self.xpos = self.ADC_ym.read()*(self.xdist/4095)-self.xcdist
            return self.xpos
        
        if coordinate == 2:
            self.yp = pyb.Pin(self.Pinyp, pyb.Pin.OUT_PP)
            self.yp.high()
            self.ym = pyb.Pin(self.Pinym, pyb.Pin.OUT_PP)
            self.ym.low()
            self.xp = pyb.Pin(self.Pinxp, pyb.Pin.IN)
            self.xm = pyb.Pin(self.Pinxm, pyb.Pin.IN)
            self.ADC_xm = pyb.ADC(self.Pinxm)
            self.ypos = self.ADC_xm.read()*(self.ydist/4095)-self.ycdist
            return self.ypos
        
        if coordinate == 3:
            self.yp = pyb.Pin(self.Pinyp, pyb.Pin.OUT_PP)
            self.yp.high()
            self.xm = pyb.Pin(self.Pinxm, pyb.Pin.OUT_PP)
            self.xm.low()
            self.xp = pyb.Pin(self.Pinxp, pyb.Pin.IN)
            self.ym = pyb.Pin(self.Pinym, pyb.Pin.IN)
            self.ADC_z = pyb.ADC(pyb.Pin.cpu.A0)
            self.zpos = self.ADC_z.read()
            if self.zpos < 3900:
                self.z = True
            else:
                self.z = False
            return self.z
    #     self.Pinxm = pyb.Pin(self.Pinxm, pyb.Pin.OUT_PP)
    #     self.Pinym = pyb.Pin(self.Pinym, pyb.Pin.IN)
    #     self.Pinxp = pyb.Pin(self.Pinxp, pyb.Pin.OUT_PP)
    #     self.Pinyp = pyb.Pin(self.Pinyp, pyb.Pin.IN)
    #     self.Pinxm.low()
    #     self.Pinxp.high()
    #     utime.sleep_us(4)
    #     self.adcx = pyb.ADC(self.Pinym)
    #     self.valx = self.adcx.read()
    #     self.corrvalx = (self.valx*self.xdist/4095) - self.xcdist
    #     return self.corrvalx 
    
    # def yscan(self):
    #     '''
    #     @brief     This function performs an y-scan of the touchpanel.
    #     @details   This function performs an y-scan of the touhpanel by setting 
    #                pins xm and xp to inputs and pins ym and yp to outputs. Pin 
    #                ym is set to low and yp is set high. The voltage is measured 
    #                at pin xm.
    #     @return    corrvaly
    #     '''
    #     self.Pinxm = pyb.Pin(self.Pinxm, pyb.Pin.IN)
    #     self.Pinym = pyb.Pin(self.Pinym, pyb.Pin.OUT_PP)
    #     self.Pinxp = pyb.Pin(self.Pinxp, pyb.Pin.IN)
    #     self.Pinyp = pyb.Pin(self.Pinyp, pyb.Pin.OUT_PP)
    #     self.Pinym.low()
    #     self.Pinyp.high()
    #     utime.sleep_us(4)
    #     self.adcy = pyb.ADC(self.Pinxm)
    #     self.valy = self.adcy.read()
    #     self.corrvaly = (self.valy*self.ydist/4095) - self.ycdist
    #     return self.corrvaly 
    
    # def zscan(self):
    #     '''
    #     @brief     This function performs an z-scan of the touchpanel.
    #     @details   This function performs an z-scan of the touhpanel by setting 
    #                pins ym and xp to inputs and pins xm and yp to outputs. Pin 
    #                xm is set to low and yp is set high. The voltage is measured 
    #                at pin xp.
    #     @return    corrvalx
    #     '''
    #     self.Pinxm = pyb.Pin(self.Pinxm, pyb.Pin.OUT_PP)
    #     self.Pinym = pyb.Pin(self.Pinym, pyb.Pin.IN)
    #     self.Pinxp = pyb.Pin(self.Pinxp, pyb.Pin.IN)
    #     self.Pinyp = pyb.Pin(self.Pinyp, pyb.Pin.OUT_PP)
    #     self.Pinxm.low()
    #     self.Pinyp.high()
    #     utime.sleep_us(4)
    #     self.adcz = pyb.ADC(self.Pinxp)
    #     self.valz = self.adcz.read()
    #     if self.valz < 4000:
    #         self.valz = True
    #     else:
    #         self.valz = False
    #     return self.valz
    
    # def scan(self):
    #     '''
    #     @brief     This function combines all the scan data.
    #     @details   This function combines all the scan dtat into a tuple. 
    #     @return    scan_tup
    #     '''
    #     self.scan_tup = (self.corrvalx, self.corrvaly, self.valz)
    #     return self.scan_tup
        
        